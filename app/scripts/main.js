(function($, window) {
	'use strict';

	const $slick = $('.slick');
	function checkWidth() {
    var windowsize = $(window).width();
    if (windowsize < 768) {
      if (!$slick.hasClass('slick-initialized')) {
        $slick.slick({
          infinite: false,
          adaptiveHeight: true,
          dots: true,
          speed: 300,
          slidesToShow: 1
        });
      }
    } else {
      if ($slick.hasClass('slick-initialized')) {
        $slick.slick('unslick');
      }
    }
  }
  // Execute on load
  checkWidth();
  // Bind event listener
  $(window).resize(checkWidth);



  // multi step form with validation 
  const $form = $('.form');

  $form.each(function(index, element) {
    const $sections = $(element).find('.form__step'),
      $navigation = $(element).find('.form__navigation'),
      $formErrorText = $(element).find('.form-error-text'),
      $formNavSteps = $(element).find('.form__navigation__steps');

    function navigateTo(index) {
      // current fieldset
      $sections.removeClass('current').eq(index).addClass('current');
      $formNavSteps.children().eq(index).addClass('completed');
    }

    function curIndex() {
      return $sections.index($sections.filter('.current'));
    }

    function activateButtons(index, element) {
      var $steps = $('.form__navigation__steps');
      $steps.children().removeClass('rendered').removeClass('inactive').eq(index-1).addClass('rendered');
      $steps.children().eq(index+1).addClass('rendered').addClass('inactive');
      $steps.children().eq(index).next().next().removeClass('rendered');
      $steps.children().eq(index).next().nextAll().addClass('inactive');

      if (index === element.length-1) {
        $navigation.hide();
      }
    }

    // Previous button is easy
    $formNavSteps.on('click', 'button', function() {
      var $this= $(this);
      console.log($this.index());
      console.log(curIndex());
      console.log($this.index() === curIndex());
      var validate = ($this.index() === curIndex()-1) ? true : false,
          validate2 =($this.index() === curIndex()+1) ? true : false;

      if ($this.hasClass('completed') && validate2) {
        $navigation.find('[type="submit"]').eq(0).trigger('click');
        setTimeout(function() {
          if ($formErrorText.is(':hidden')) {
            navigateTo($this.index());
            activateButtons(curIndex(), $this.parents('.form').find('.form__step'));
          }
        },0);
      }

      if ($this.hasClass('completed') && validate) {
        $formErrorText.removeClass('show');
        navigateTo($this.index());
        activateButtons(curIndex(), $this.parents('.form').find('.form__step'));
      }
      return false;
    });

    // Next button goes forward if current block validates
    let listOfErrorLabels = [];
    window.Parsley.on('field:error', fieldInstance => {
      let arrErrorMsg = ParsleyUI.getErrorsMessages(fieldInstance);
      let errorMsg = arrErrorMsg.join(';');
      let fieldName = fieldInstance.$element.parents('.form__wrap').find('label').text();
      listOfErrorLabels.push(fieldName);
      listOfErrorLabels = _.union(listOfErrorLabels);
    });

    $navigation.on('click', '[type="submit"]', function(e) {
      e.preventDefault();
      listOfErrorLabels.length = 0;
     
      if ($(this).parents('.form').parsley({
        successClass: 'form-success',
        errorClass: 'form-error',
        classHandler: function (el) {
          return el.$element.parents('.form__wrap');
        },
        errorsWrapper: '',
        errorsContainer: function(parsleyField) {}
      }).validate({group: 'block-' + curIndex()})) {
        navigateTo(curIndex() + 1);
        $(this).parents('.form').find('.form-error-text').removeClass('show');
        activateButtons(curIndex(), $(this).parents('.form').find('.form__step'));
      }

      // check if there are any errors 
      // if yes then show a error text with list of field that are missed
      if (listOfErrorLabels.length > 0) {
        let updatedList = listOfErrorLabels.reduce((template, text, index) => {
          return template = `${template} &nbsp;<span>${text}</span>&nbsp;`;
        }, '');
        $(this).parents('.form').find('.form-error-text').addClass('show').html(`Ошибка при заполнении полей ${updatedList}`);
      } else {
        $(this).parents('.form').find('.form-error-text').removeClass('show');
      }
    });

    $sections.each((index, section) => {
      $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });

    navigateTo(0);

  });
  

  $('input[data-type="phone"]').inputmask({'mask': '+375-99-999-99-99'});
  $('input[data-type="numbers"]').inputmask('9{14}', {
    'placeholder' : ''
  });
  $('input[data-type="year"]').inputmask('9{4}', {
    'placeholder' : ''
  });
  $('input[data-type="month"]').inputmask('9{2}', {
    'placeholder' : ''
  });
  $('input[data-type="passport"]').inputmask('aa9{7}', {
    'placeholder' : ''
  });
  $('input[data-type="ident"]').inputmask({
    'mask':'9{7} a 9{3} aa 9',
    'placeholder' : ''
  });

  $('input.day').on('keyup', function(e) {
    var value = e.target.value;
    if (value > 31) {
      e.target.value = 31;
    } 
  });
  $('input.month').on('keyup', function(e) {
    var value = e.target.value;
    if (value > 12) {
      e.target.value = 12;
    } 
  });

  $('input[data-type="letters"]').on('input', function(event) {
    var inputText = $(this).val();

    var resultText = inputText.replace(/[^а-яё]/gi, '');
    $(this).val(resultText);
  });


  $(document).on('keypress', function(evt) {
    if(evt.isDefaultPrevented()) {
      // Assume that's because of maskedInput
      // See https://github.com/guillaumepotier/Parsley.js/issues/1076
      $(evt.target).trigger('input');
    }
  });

  var $navbarBrandAnimate = $('.navbar-brand-animate');

  if ($navbarBrandAnimate.length) {
    $(window).on('scroll', function() {
      var windowTop = $(window).scrollTop(),
          targetElement= $('.hero .hero__logo'),
          targetElementTop = targetElement.offset().top,
          targetElementHeight = targetElement.height();

      if (windowTop > (targetElementTop+targetElementHeight)) {
        $navbarBrandAnimate.addClass('active');
      } else {
        $navbarBrandAnimate.removeClass('active');
      }
    });
  }


  // stick when scrolling
  var $stickyBlock = $('.content__bfamily');
  // $('.content__bfamily').sticky();
  if ($stickyBlock.length) {
    var stickyTop = $stickyBlock.offset().top,
        stickyLeft = $stickyBlock.offset().left,
        stickyHeight = $stickyBlock.outerHeight(),
        leftPosition = $stickyBlock.parent().offset().left + $stickyBlock.position().left,
        number = 30;

    function scrollEvent() {
      $(window).scroll(function(){
        var limit = $('.main-footer').offset().top - stickyHeight,
            windowTop = $(window).scrollTop()+number;

        if (stickyTop < windowTop){
          $stickyBlock.css({ position: 'fixed', top: '30px'});
        } else {
          $stickyBlock.css({ position: 'static'});
        }

        if (limit < windowTop+number) {
          var diff = limit - windowTop;
          $stickyBlock.css({top: diff});
        }
      });
    }
    scrollEvent();
  }

  // position of input
  function doGetCaretPosition(oField) {
    // Initialize
    var iCaretPos = 0;
    // IE Support
    if (document.selection) {
      oField.focus();
      var oSel = document.selection.createRange ();
      oSel.moveStart ('character', -oField.value.length);
      iCaretPos = oSel.text.length;
    } else if (oField.selectionStart || oField.selectionStart == '0') {
      iCaretPos = oField.selectionStart;
    }

    return (iCaretPos);
  }


  $('input').on('keypress keydown', function(e) {
    var tabkey = 9;
    if(e.keyCode == tabkey) {
      $(this).closest('input').focus();
    }
  });

  $('.date-custom input').on('focus keypress keyup keydown', function() {
    var $this = $(this);
    switchInput.call($this[0]);
  });

  // switches between inputs
  function switchInput() {
    var $this = $(this),
        max = $this.attr('maxlength') || 2,
        currentLength = $this.val().length,
        key = event.keyCode || event.charCode,
        dataPrevLength;

    if (event.type === 'keydown') {
      $this.attr('data-prev', $this.val());
      dataPrevLength = $this.attr('data-prev').length;
    }
    if (event.type === 'keyup') {
      var position = doGetCaretPosition($this[0]);
      if (position === 0 && (key == 8 || key == 46)) {
        $this.prev('input').focus();
      }

      if (position === 0 && dataPrevLength == 0 && currentLength === 0 && (key == 8 || key == 46)) {
        $this.prev('input').focus();
      } 

      if ((key !== 37 || key !== 39) && currentLength === max-- && position === 2) {
        $this.next('input').focus();
      }
    }
  }

  // custom input file
  var inputs = document.querySelectorAll( '.file-custom input[type="file"]' );
	Array.prototype.forEach.call( inputs, function( input ) {
		var label	 = input.parentNode,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e ) {
			var fileName = '',
          target = e.target,
          that = this;

      var file = target.files[0];
    
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'strong' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;


      if (file) {
        var r = new FileReader();
        r.onload = function(e) {
          var sizeMB = file.size/1024/1024;
          if (sizeMB.toFixed(2) > 20) {
            target.value = '';
            label.querySelector( 'strong' ).innerHTML = '';
            return false;
          }
        }
        r.readAsText(file);
      }
		});
	});

  // Test for background clip text
  Modernizr.addTest('backgroundcliptext',function(){
    var div = document.createElement('div');
    div.style.webkitBackgroundClip = 'text';
    var text = div.style.cssText.indexOf('text');
    if (text > 0) return true;
    'Webkit Moz O ms Khtml'.replace(/([A-Za-z]*)/g,function(val){ 
        if (val+'BackgroundClip' in div.style) return true;
    });
  });

  if (!Modernizr.backgroundcliptext) {
    $('.txt-gradient').css({background:'transparent'});
  }


  // Cache selectors
  var lastId,
      topMenu = $('.dropdown ul'),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find('a'),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr('href'));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  $('a[data-animate]').on('click', function(e) {
    var target = $(this).data('animate');
    var offsetTop = $(target).offset().top;
  
    if (target.length) {
      e.preventDefault();
      $('html, body').animate({
        scrollTop: offsetTop
      }, 300);
    }
  });

  $('.dropdown ul a').click(function(e){
    var href = $(this).attr('href'),
      offsetTop = href === '#' ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
      scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
    // Get container scroll position
    var fromTop = $(this).scrollTop()+topMenuHeight; 
    // Get id of current scroll item
    var cur = scrollItems.map(function(){
      if ($(this).offset().top < fromTop)
        return this;
    });
    // Get the id of the current element
    cur = cur[cur.length-1];
    var id = cur && cur.length ? cur[0].id : '';
     
    if (lastId !== id) {
        lastId = id;
        menuItems
          .parent().removeClass('active')
          .end().filter('[href=\'#'+id+'\']').parent().addClass('active');
    }                   
  });


})(jQuery, window);